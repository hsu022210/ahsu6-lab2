package cs601.shapes;
import java.util.Comparator;

/**
 * Created by Alec on 9/18/2016.
 *
 * A comparator that compares Shapes based on the volume
 */
public class VolumeComparator implements Comparator<Shape> {
    /** Compare the two shapes by comparing their volumes.
     * @param o1
     * 		The first shape
     * @param o2
     * 		The second shape
     * @return An integer result. A negative number is returned if o1 is less than o2,
     * a positive number means o2 is less than o1. 0 if both shapes are "equal".
     * */
    @Override
    public int compare(Shape o1, Shape o2) {
        // FILL IN CODE

        double o1Volume = ((Shape3D)o1).volume();
        double o2Volume = ((Shape3D)o2).volume();

        if (Math.abs(o1Volume - o2Volume) <= 0.001) {
            return 0;

        } else if (o1Volume > o2Volume) {
            return 1;

        } else {
            return -1;

        }
    }

}
