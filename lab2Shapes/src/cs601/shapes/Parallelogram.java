package cs601.shapes;

/**
 * Created by Alec on 9/16/2016.
 *
 * An class Parallelogram. Extends Shape2D.
 */
public class Parallelogram extends Shape2D {
    private double length1;
    private double length2;
    private double angle;

    public Parallelogram(double length1, double length2, double angle){
        this.length1 = length1;
        this.length2 = length2;
        this.angle = angle;
    }

    /**
     * A method for computing the area or the surface area of the shape.
     * @return area of the shape
     */
    public double area(){
        double result = length1 * length2 * Math.sin(Math.toRadians(angle));
        return result;
    };

    /**
     * A method for computing the perimeter of the shape.
     * @return The perimeter of the shape
     */
    public double perimeter(){
        double result = 2 * (length1 + length2);
        return result;
    };
}
