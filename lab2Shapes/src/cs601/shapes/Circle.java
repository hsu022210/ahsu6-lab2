package cs601.shapes;

/**
 * Created by Alec on 9/16/2016.
 *
 * An class Circle. Extends Shape2D.
 */
public class Circle extends Shape2D {
    private double radius;

    public Circle(double radius){
        this.radius = radius;
    }

    /**
     * A method for computing the area or the surface area of the shape.
     * @return area of the shape
     */
    public double area(){
        double result = this.radius * this.radius * Math.PI;
        return result;
    };


    /**
     * A method for computing the perimeter of the shape.
     * @return The perimeter of the shape
     */
    public double perimeter(){
        double result = this.radius * 2 * Math.PI;
        return result;
    };

}
