package cs601.shapes;

/**
 * Created by Alec on 9/18/2016.
 *
 * An class Parallelepiped. Extends Shape3D.
 */
public class PlatonicSolid extends Shape3D{
    private double numOfEdgesEachFace;
    private double numOfFacesEachVertex;
    private double length;

    public PlatonicSolid(double numOfEdgesEachFace, double numOfFacesEachVertex, double length){
        this.numOfEdgesEachFace = numOfEdgesEachFace;
        this.numOfFacesEachVertex = numOfFacesEachVertex;
        this.length = length;
    }

    /**
     * A method for computing the area or the surface area of the shape.
     * @return area of the shape
     */
    public double area(){
        double down = 4-(numOfEdgesEachFace-2) * (numOfFacesEachVertex-2);
        double f = (4 * numOfFacesEachVertex) / down;
        double result = (length/2) * (length/2) * f * numOfEdgesEachFace * (1/Math.tan(Math.PI/numOfEdgesEachFace));
        return result;
    };

    /**
     * A method for computing the volume of the shape.
     * @return A volume of the 3D shape.
     */
    public double volume(){
        double dihedralAngle = 2*Math.asin(Math.cos(Math.PI / numOfFacesEachVertex) / Math.sin(Math.PI / numOfEdgesEachFace));
        double inRadius = (length/2) * (1/Math.tan(Math.PI/numOfEdgesEachFace)) * (Math.tan(dihedralAngle/2));
        double result = (1.0/3.0) * inRadius * this.area();
        return result;
    };
}
