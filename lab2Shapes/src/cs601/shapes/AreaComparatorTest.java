package cs601.shapes;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class AreaComparatorTest {

	/** This test will call the comparator and sort a list.
	 */
	@Test
	public void testComparator() {

		String testName = "testComparator";
		List expectedList = new ArrayList();
		List outputList = new ArrayList();

		Parallelogram testObject1 = new Parallelogram(1.0, 1.0, 30.0);
		ConvexRegularPolygon testObject2 = new ConvexRegularPolygon(3, 3.0);
		Circle testObject3 = new Circle(2.5);

		expectedList.add(testObject1);
		expectedList.add(testObject2);
		expectedList.add(testObject3);

		outputList.add(testObject3);
		outputList.add(testObject1);
		outputList.add(testObject2);

		Collections.sort(outputList, new AreaComparator());

		Assert.assertEquals(String.format("%n" + "Test Case: %s%n", testName), expectedList, outputList);
	}
	
}


