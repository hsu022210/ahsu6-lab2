package cs601.shapes;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import org.junit.Assert;
import org.junit.Test;

public class CircleTest {

	/** This test will call the area method.
	 */
	@Test
	public void testArea() {

		String testName = "testArea";
		Circle testObject = new Circle(2.5);

		double eps = 0.001;
		double expectedArea = 19.63;
		double outputArea = decimalDigit(testObject.area());

		Assert.assertEquals(String.format("%n" + "Test Case: %s%n", testName), expectedArea, outputArea, eps);
	}


	/** This test will call the perimeter method.
	 */
	@Test
	public void testPerimeter() {

		String testName = "testPerimeter";
		Circle testObject = new Circle(2.5);

		double eps = 0.001;
		double expectedPerimeter = 15.71;
		double outputPerimeter = decimalDigit(testObject.perimeter());

		Assert.assertEquals(String.format("%n" + "Test Case: %s%n", testName), expectedPerimeter, outputPerimeter, eps);

	}

	public double decimalDigit(double num) {
		DecimalFormat df = new DecimalFormat("#.00");
		String resultString = df.format(num);
		double result = Double.parseDouble(resultString);
		return result;
	}
	
}


