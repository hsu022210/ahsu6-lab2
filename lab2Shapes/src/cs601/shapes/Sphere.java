package cs601.shapes;

/**
 * Created by Alec on 9/18/2016.
 *
 * An class Sphere. Extends Shape3D.
 */
public class Sphere extends Shape3D{
    private double radius;

    public Sphere(double radius){
        this.radius = radius;
    }

    /**
     * A method for computing the area or the surface area of the shape.
     * @return area of the shape
     */
    public double area(){
        double result = 4 * Math.PI * radius * radius;
        return result;
    };

    /**
     * A method for computing the volume of the shape.
     * @return A volume of the 3D shape.
     */
    public double volume(){
        double result = Math.PI * radius * radius * radius * 4/3;
        return result;
    };
}
