package cs601.shapes;
import java.util.Comparator;

/**
 * Created by Alec on 9/18/2016.
 *
 * A comparator that compares Shapes based on the perimeter
 */
public class PerimeterComparator implements Comparator<Shape> {
    /** Compare the two shapes by comparing their perimeters.
     * @param o1
     * 		The first shape
     * @param o2
     * 		The second shape
     * @return An integer result. A negative number is returned if o1 is less than o2,
     * a positive number means o2 is less than o1. 0 if both shapes are "equal".
     * */
    @Override
    public int compare(Shape o1, Shape o2) {
        // FILL IN CODE
        double o1Perimeter = ((Shape2D)o1).perimeter();
        double o2Perimeter = ((Shape2D)o2).perimeter();

        if (Math.abs(o1Perimeter - o2Perimeter) <= 0.001) {
            return 0;

        } else if (o1Perimeter > o2Perimeter) {
            return 1;

        } else {
            return -1;

        }
    }

}
