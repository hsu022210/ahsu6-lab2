package cs601.shapes;

import javax.jws.Oneway;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardOpenOption.*;
import java.text.NumberFormat;
import java.text.ParseException;
import java.lang.ArrayIndexOutOfBoundsException;

/** A class that stores a collection of shapes, and is able to sort 
 * shapes based on different criteria. Stores shapes in three lists:
 * allShapes, shapes2D, shapes3D. 
 * For general shapes: sorting is by area or by name.
 * For 2d shapes, sorting is by area, by name or by perimeter.
 * For 3d shapes, sorting is by area, by name or by volume.
 * @author okarpenko
 *
 */
public class ShapeSorter {
	
	// FILL IN CODE:
	// Store in three data structures: 1) all shapes; 2) all 2D shapes;  3) all 3D shapes
	private List<Shape> ShapeallList;
	private List<Shape2D> Shape2DList;
	private List<Shape3D> Shape3DList;
	
	/** A default constructor for class ShapeSorter */
	public ShapeSorter() {
		// FILL IN CODE
		// Initialize the data structures
		ShapeallList = new ArrayList<Shape>();
		Shape2DList = new ArrayList<Shape2D>();
		Shape3DList = new ArrayList<Shape3D>();
	}

	/** Read a given file, create different shapes, and add them to the data structures
	 *  @param filename
	 *  	      The name of the file that contains info about the shapes.
	 */
	public void loadShapes(String filename) {
		// FILL IN CODE
		try (FileReader f = new FileReader(filename))
		{
			BufferedReader br = new BufferedReader(f);
			String line;
			while ((line = br.readLine()) != null) {

				String tempClassName;
				Object tempClass = new Object();
				List tempParamList = new ArrayList();

				String[] parts = line.split(": ");

				tempClassName = parts[0];

				try {
					String[] parameters = parts[1].split(" ");
					for (String param: parameters) {

						double arguDoubleType = Double.parseDouble(param);

						tempParamList.add(arguDoubleType);

					}
				}catch (ArrayIndexOutOfBoundsException e){
					e.printStackTrace();
				}


				switch (tempClassName) {
					case "Circle":
						tempClass = new Circle((double)tempParamList.get(0));
						Shape2DList.add((Shape2D)tempClass);
						ShapeallList.add((Shape) tempClass);
						break;

					case "Parallelogram":
						tempClass = new Parallelogram((double)tempParamList.get(0), (double)tempParamList.get(1), (double)tempParamList.get(2));
						Shape2DList.add((Shape2D)tempClass);
						ShapeallList.add((Shape) tempClass);
						break;

					case "ConvexRegularPolygon":
						tempClass = new ConvexRegularPolygon((double)tempParamList.get(0), (double)tempParamList.get(1));
						Shape2DList.add((Shape2D)tempClass);
						ShapeallList.add((Shape) tempClass);
						break;

					case "Sphere":
						tempClass = new Sphere((double)tempParamList.get(0));
						Shape3DList.add((Shape3D)tempClass);
						ShapeallList.add((Shape) tempClass);
						break;

					case "Parallelepiped":
						tempClass = new Parallelepiped((double)tempParamList.get(0), (double)tempParamList.get(1), (double)tempParamList.get(2), (double)tempParamList.get(3), (double)tempParamList.get(4));
						Shape3DList.add((Shape3D)tempClass);
						ShapeallList.add((Shape) tempClass);
						break;

					case "PlatonicSolid":
						tempClass = new PlatonicSolid((double)tempParamList.get(0), (double)tempParamList.get(1), (double)tempParamList.get(2));
						Shape3DList.add((Shape3D)tempClass);
						ShapeallList.add((Shape) tempClass);
						break;

					default:
						System.out.println("shape not exist when loading");
						break;
				}


//				if (tempClassName.equals("Circle") || tempClassName.equals("Parallelogram") || tempClassName.equals("ConvexRegularPolygon")){
//					switch (tempClassName) {
//						case "Circle":
//							tempClass = new Circle((double)tempParamList.get(0));
//							break;
//
//						case "Parallelogram":
//							tempClass = new Parallelogram((double)tempParamList.get(0), (double)tempParamList.get(1), (double)tempParamList.get(2));
//							break;
//
//						case "ConvexRegularPolygon":
//							tempClass = new ConvexRegularPolygon((double)tempParamList.get(0), (double)tempParamList.get(1));
//							break;
//
//						default:
//							System.out.println("shape2D not exist when loading");
//							break;
//					}
//
//					Shape2DList.add((Shape2D)tempClass);
//
//				}else {
//					switch (tempClassName) {
//						case "Sphere":
//							tempClass = new Sphere((double)tempParamList.get(0));
//							break;
//
//						case "Parallelepiped":
//							tempClass = new Parallelepiped((double)tempParamList.get(0), (double)tempParamList.get(1), (double)tempParamList.get(2), (double)tempParamList.get(3), (double)tempParamList.get(4));
//							break;
//
//						case "PlatonicSolid":
//							tempClass = new PlatonicSolid((double)tempParamList.get(0), (double)tempParamList.get(1), (double)tempParamList.get(2));
//							break;
//
//						default:
//							System.out.println("shape3D not exist when loading");
//							break;
//					}
//
//					System.out.println(tempClass.getClass().getSimpleName());
//					Shape3DList.add((Shape3D)tempClass);
//				}
//
//				ShapeallList.add((Shape) tempClass);
//				System.out.println(line);
			}

		} catch (IOException e) {
			System.out.print(e.getMessage());
		}
	}

	
	/** Sort a list of Shape-s 
	 * @param whichShapes
	 * 		 A string, either "all", "2D" or "3D" - specifies which list of shapes to sort
	 * @param comparator
	 * 		 A Comparator object that tells the method how to sort shapes.
	 */
	public void sortShapes(String whichShapes, Comparator<Shape> comparator) {
		// FILL IN CODE
		List tempList = new ArrayList<Shape>();

		switch (whichShapes) {
			case "2D":
				tempList = Shape2DList;
				break;

			case "3D":
				tempList = Shape3DList;
				break;

			case "all":
				tempList = ShapeallList;
				break;

			default:
				System.out.println("shape type not exist when sorting");
				break;
		}

		Collections.sort(tempList, comparator);
		
	}

	
	/** Print the list of shapes to a file as following:
	 * First, all 2d shapes (one line per shape),  followed by an empty line. 
	 * Then all 3d shapes,  followed  by an empty line.. 
	 * Then all shapes from the list. Each shape should be printed according to the format
	 * specified in the toString() method of the corresponding parent class
	 * @param filename
	 * 	    The name of the file where the results are going to be printed.
	 */
	public void printToFile(String filename) {
		// FILL IN CODE
		Path filenamePathType = Paths.get(filename);

		try (OutputStream out = new BufferedOutputStream(
				Files.newOutputStream(filenamePathType, CREATE, APPEND))) {

			String newLine = "\n";
			byte newLineBytes[] = newLine.getBytes();


			for (Shape2D theShape: Shape2DList){
				String result = "";
				result += theShape.toString();
				result += "\n";

				byte data[] = result.getBytes();
				out.write(data, 0, data.length);
			}
			out.write(newLineBytes, 0, newLineBytes.length);


			for (Shape3D theShape: Shape3DList){
				String result = "";
				result += theShape.toString();
				result += "\n";

				byte data[] = result.getBytes();
				out.write(data, 0, data.length);
			}
			out.write(newLineBytes, 0, newLineBytes.length);


			for (Shape theShape: ShapeallList){
				String result = "";
				result += theShape.toString();
				result += "\n";

				byte data[] = result.getBytes();
				out.write(data, 0, data.length);
			}
			out.write(newLineBytes, 0, newLineBytes.length);


		} catch (IOException x) {
			System.err.println(x);
		}

	}

}
