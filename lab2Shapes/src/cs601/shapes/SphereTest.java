package cs601.shapes;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import org.junit.Assert;
import org.junit.Test;

public class SphereTest {

	/** This test will call the area method.
	 */
	@Test
	public void testArea() {

		String testName = "testArea";
		Sphere testObject = new Sphere(4.0);

		double eps = 0.001;
		double expectedArea = 201.06;
		double outputArea = decimalDigit(testObject.area());

		Assert.assertEquals(String.format("%n" + "Test Case: %s%n", testName), expectedArea, outputArea, eps);
	}


	/** This test will call the volume method.
	 */
	@Test
	public void testVolume() {

		String testName = "testVolume";
		Sphere testObject = new Sphere(4.0);

		double eps = 0.001;
		double expectedVolume = 268.08;
		double outputVolume = decimalDigit(testObject.volume());

		Assert.assertEquals(String.format("%n" + "Test Case: %s%n", testName), expectedVolume, outputVolume, eps);

	}

	public double decimalDigit(double num) {
		DecimalFormat df = new DecimalFormat("#.00");
		String resultString = df.format(num);
		double result = Double.parseDouble(resultString);
		return result;
	}
	
}


