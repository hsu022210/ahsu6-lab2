package cs601.shapes;

/**
 * Created by Alec on 9/18/2016.
 *
 * An class ConvexRegularPolygon. Extends Shape2D.
 */
public class ConvexRegularPolygon extends Shape2D {
    private double numOfEdges;
    private double length;

    public ConvexRegularPolygon(double numOfEdges, double length){
        this.numOfEdges = numOfEdges;
        this.length = length;
    }

    /**
     * A method for computing the area or the surface area of the shape.
     * @return area of the shape
     */
    public double area(){
        double result = (1.0/4.0) * numOfEdges * length * length * (1/Math.tan(Math.PI/numOfEdges));
        return result;
    };

    /**
     * A method for computing the perimeter of the shape.
     * @return The perimeter of the shape
     */
    public double perimeter(){
        double result = numOfEdges * length;
        return result;
    };
}
