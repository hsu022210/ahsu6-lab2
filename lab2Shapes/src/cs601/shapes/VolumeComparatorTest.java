package cs601.shapes;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

public class VolumeComparatorTest {

	/** This test will call the comparator and sort a list.
	 */
	@Test
	public void testComparator() {

		String testName = "testComparator";
		List expectedList = new ArrayList();
		List outputList = new ArrayList();

		PlatonicSolid testObject3 = new PlatonicSolid(5, 3, 4.0);
		Parallelepiped testObject1 = new Parallelepiped(1.0, 2.0, 4.0, 45.0, 45.0);
		Sphere testObject2 = new Sphere(4.0);

		expectedList.add(testObject1);
		expectedList.add(testObject2);
		expectedList.add(testObject3);

		outputList.add(testObject3);
		outputList.add(testObject1);
		outputList.add(testObject2);

		Collections.sort(outputList, new VolumeComparator());

		Assert.assertEquals(String.format("%n" + "Test Case: %s%n", testName), expectedList, outputList);
	}
	
}


