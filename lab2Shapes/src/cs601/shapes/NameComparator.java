package cs601.shapes;

import java.util.Comparator;

/**
 * Created by Alec on 9/18/2016.
 *
 * A comparator that compares Shapes based on the name
 */
public class NameComparator implements Comparator<Shape> {
    /** Compare the two shapes by comparing their names.
     * @param o1
     * 		The first shape
     * @param o2
     * 		The second shape
     * @return An integer result. A negative number is returned if o1 is less than o2,
     * a positive number means o2 is less than o1. 0 if both shapes are "equal".
     * */
    @Override
    public int compare(Shape o1, Shape o2) {
        // FILL IN CODE
        String o1Name = o1.getClass().getSimpleName();
        String o2Name = o2.getClass().getSimpleName();
        int result = o1Name.compareTo(o2Name);

        return result; // don't forget to change it
    }

}
