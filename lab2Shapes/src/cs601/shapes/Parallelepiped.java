package cs601.shapes;

/**
 * Created by Alec on 9/18/2016.
 *
 * An class Parallelepiped. Extends Shape3D.
 */
public class Parallelepiped extends Shape3D {
    private double length1;
    private double length2;
    private double length3;
    private double angle12;
    private double angle23;

    public Parallelepiped(double length1, double length2, double length3, double angle12, double angle23){
        this.length1 = length1;
        this.length2 = length2;
        this.length3 = length3;
        this.angle12 = angle12;
        this.angle23 = angle23;
    }

    /**
     * A method for computing the area or the surface area of the shape.
     * @return area of the shape
     */
    public double area(){
        double result = 2 * (length1 * length2 * Math.sin(Math.toRadians(angle12))+ length2 * length3 * Math.sin(Math.toRadians(angle23)) + length1 * length3 * Math.sin(Math.toRadians(angle23)));
        return result;
    };

    /**
     * A method for computing the volume of the shape.
     * @return A volume of the 3D shape.
     */
    public double volume(){
        double cos12 = Math.cos(Math.toRadians(angle12));
        double cos23 = Math.cos(Math.toRadians(angle23));
        double inside = 1 + 2*cos12*cos23*cos23 - cos12*cos12 - cos23*cos23 - cos23*cos23;
        double result = length1 * length2 * length3 * Math.sqrt(inside);
        return result;
    };
}
